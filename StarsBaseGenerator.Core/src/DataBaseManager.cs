﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace StarsBaseGenerator.Core
{
    public class DataBaseManager
    {       
        private readonly StarsLoader _starsLoader;

        public DataBaseManager(StarsLoader starsLoader)
        {
            _starsLoader = starsLoader;
        }

        public void CreateDataBase(int level, string inputPath, string outputPath)
        {
            var points = _starsLoader.Load(inputPath);
            var tree = new SphericalTriangleTree(level);

            var mapLeafsWithStars = getLeafsWithStars(tree, points);
            CreateDataBaseFile(mapLeafsWithStars, outputPath);

            var mapNodesOffsets = GetNodesOffsets(tree, mapLeafsWithStars);
            var mapNodeNeigbors = GetNodeNeighbors(tree);

            CreateIndexFile(mapNodeNeigbors, mapNodesOffsets, GetIndexPath(outputPath));

            //string preCompileCppFileName = @"C:\Users\Saraev\Documents\work\CometProject\ActualStarsBaseGenerator\StarsFinder\StarsFinder\PreCompileTree.cpp";
            //PreCompileCppTreeCreator.CreateTreeStructInCppFile(tree, preCompileCppFileName);
        }

        private void CreateDataBaseFile(Dictionary<Node, List<SphericalPointWithProperties>> leafsWithStars, string outputPath)
        {
            SphericalPointWithProperties anyPoint = getFirstNotNullPoint(leafsWithStars); //need for structure point
            WriteDescriptor(CreateDescriptorDb(anyPoint), outputPath);  

            using (var writer = new BinaryWriter(File.Open(outputPath, FileMode.Append)))
            {                        
                foreach (Node key in leafsWithStars.Keys.OrderBy(x => x.Id))
                {
                    foreach (SphericalPointWithProperties p in leafsWithStars[key])
                    { 
                        writer.Write(p.Latitude);
                        writer.Write(p.Longitude);
                        foreach (var pair in p.Properties)
                        {
                            if (pair.Value is string)
                                writer.Write(pair.Value.ToCharArray());
                            else
                                writer.Write(pair.Value);
                        }
                    }
                }
            }
        }
        private SphericalPointWithProperties getFirstNotNullPoint(Dictionary<Node, List<SphericalPointWithProperties>> dictionary)
        {
            SphericalPointWithProperties point = null;
            foreach (var pair in dictionary)
            {
                if (pair.Value.Count > 0) 
                { 
                    point = pair.Value[0];
                    break;
                }
            }
            if (point != null) return point;
            throw new NullReferenceException();
        }

        private void WriteDescriptor(DescriptorDb desc, string outputPath)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(outputPath, FileMode.Create)))
            {
                writer.Write(desc.PropertiesCount);
                foreach (var pair in desc.Properties)
                {
                    writer.Write(pair.Key);
                    writer.Write((byte)pair.Value.Item1);
                    if (pair.Value.Item1 == TypeCode.String)
                        writer.Write(pair.Value.Item2);
                }

                writer.Write(desc.PointSize);
                writer.Write(desc.Size);
            }
        }

        private DescriptorDb CreateDescriptorDb(SphericalPointWithProperties point)
        {
            var desc = new DescriptorDb();

            int descSize = 0,
                pointSizeInDb = 0;

            pointSizeInDb += sizeof(double); //lat
            pointSizeInDb += sizeof(double); //lon

            descSize += sizeof(int); //propertiesCount
            foreach (var pair in point.Properties)
            {
                TypeCode tc = Type.GetTypeCode(((Object)pair.Value).GetType());
                desc.Properties.Add(pair.Key, new Tuple<TypeCode, byte>(tc, 0));
                descSize += (pair.Key.Length + 1) * sizeof(byte); // 1 byte of length string + n bytes of string
                descSize += sizeof(byte); //TypeCode length

                if (tc == TypeCode.String)
                {
                    desc.Properties[pair.Key] = new Tuple<TypeCode, byte>(tc, pair.Value.length);
                    descSize += sizeof(byte);//string pair.Value length
                }

                pointSizeInDb += sizeofObjectType(pair.Value);
            }
            descSize += sizeof(int); //point size
            descSize += sizeof(int); //desc size

            desc.PropertiesCount = point.Properties.Count;
            desc.PointSize = pointSizeInDb;
            desc.Size = descSize;

            return desc;
        }
        private int sizeofObjectType(object o)
        {
            int size = 0;
            Type t = o.GetType();

            if (t == typeof(string))
            {
                size += ((string)o).Length*sizeof(byte);
            }
            else if (t == typeof(char)) // cause not correct work char type with Marshal.SizeOf
                size += 1;
            else
                size += Marshal.SizeOf(o);

            return size;
        }


        private void CreateIndexFile(Dictionary<Node, List<Node>> nodesNeighbors, Dictionary<int, Tuple<int, int>> nodesOffsets, string outputPath)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(outputPath, FileMode.Create)))
            {
                foreach (var pair in nodesOffsets.OrderBy(x => x.Key))
                {
                    writer.Write(pair.Value.Item1);
                    writer.Write(pair.Value.Item2);

                    var neighbors = nodesNeighbors[nodesNeighbors.Keys.First(node => node.Id == pair.Key)];
                    foreach (var node in neighbors)
                    {
                        writer.Write(node.Id);
                    }
                }
            }
        }

        private static Dictionary<int, Tuple<int, int>> GetNodesOffsets(SphericalTriangleTree tree, Dictionary<Node, List<SphericalPointWithProperties>> leafsWithStars)
        {
            var nodesOffsets = new Dictionary<int, Tuple<int, int>>();

            int offsetInStars = 0;
            foreach (var leaf in leafsWithStars.Keys.OrderBy(x => x.Id))
            {
                int starCount = leafsWithStars[leaf].Count;

                nodesOffsets.Add(leaf.Id, new Tuple<int, int>(offsetInStars, offsetInStars + starCount));
                offsetInStars += starCount;

            }
            
            for (int i = tree.Levels.Count - 2; i >= 1; i--)
            {                
                foreach (var node in tree.Levels[i])
                {
                    int leftChildStart=nodesOffsets[node.Childs[0].Id].Item1;
                    int rightChildFinish=nodesOffsets[node.Childs[node.Childs.Count-1].Id].Item2;
                    nodesOffsets.Add(node.Id, new Tuple<int, int>(leftChildStart, rightChildFinish));     
                }
            }

            return nodesOffsets;
        }

        private static void GetTreeNods(Node root, List<Node> nods)
        {
            if (nods == null) throw new ArgumentNullException("nods mustn't be null");

            if (root != null)
            {
                nods.Add(root);
                if (root.HasChild())
                    foreach (var child in root.Childs)
                        GetTreeNods(child, nods);
            }
        }

        private Dictionary<Node, List<SphericalPointWithProperties>> getLeafsWithStars(SphericalTriangleTree tree, List<SphericalPointWithProperties> points)
        {
            var leafs = tree.Levels[tree.Levels.Count - 1];

            var map = leafs.ToDictionary(leaf => leaf, leaf => new List<SphericalPointWithProperties>());

            foreach (var point in points)
            {
                var leaf = tree.FindLastHitNode(point);
                map[leaf].Add(point);
            }

            return map;
        }


        private Dictionary<Node, List<SphericalPointWithProperties>> GetNodeWithStars(SphericalTriangleTree tree, List<SphericalPointWithProperties> points)
        {
            var allNodsOfTree = new List<Node>();
            GetTreeNods(tree.MainRoot, allNodsOfTree);

            var map = allNodsOfTree.ToDictionary(nodeOfTree => nodeOfTree, nodeOfTree => new List<SphericalPointWithProperties>());

            foreach (var point in points)
            {
                var node = tree.MainRoot;
                while (node.HasChild())
                {
                    foreach (var child in node.Childs)
                    {
                        if (!child.Triangle.IsHit(point)) continue;

                        node = child;
                        if (!map.ContainsKey(node)) map.Add(node, new List<SphericalPointWithProperties>());
                        map[node].Add(point);

                        break;
                    }
                }
            }
            return map;
        }

        private Dictionary<Node, List<Node>> GetNodeNeighbors(SphericalTriangleTree tree)
        {
            var nodeNeighbors = new Dictionary<Node, List<Node>>();

            foreach (var node in tree.MainRoot.Childs)
            {
                var neighbors = GetNeighborsOfNodeFromCandidates(node, tree.MainRoot.Childs);
                nodeNeighbors.Add(node, neighbors);
            }

            for (int level = 2; level < tree.Levels.Count; level++)
            {
                foreach (var node in tree.Levels[level])
                {
                    var parent = node.Parent;
                    var neighborsOfParent = nodeNeighbors[parent];

                    var candidates = new List<Node>(parent.Childs);
                    foreach (var neighbor in neighborsOfParent)
                        candidates.AddRange(neighbor.Childs);

                    nodeNeighbors.Add(node, GetNeighborsOfNodeFromCandidates(node, candidates));
                }
            }
            return nodeNeighbors;
        }

        private static List<Node> GetNeighborsOfNodeFromCandidates(Node node, List<Node> candidates)
        {
            const int NEIGHBORS_COUNT = 3;
            var neighbors = new List<Node>();

            var pointsOfNeighbors = new List<SphericalPoint>();

            var point = Util.GetPointOnMedianOutsideTriangleInFrontOfOppositePoint(node.Triangle.Vertex1, node.Triangle.Vertex2, node.Triangle.Vertex3);
            pointsOfNeighbors.Add(point);

            point = Util.GetPointOnMedianOutsideTriangleInFrontOfOppositePoint(node.Triangle.Vertex1, node.Triangle.Vertex3, node.Triangle.Vertex2);
            pointsOfNeighbors.Add(point);

            point = Util.GetPointOnMedianOutsideTriangleInFrontOfOppositePoint(node.Triangle.Vertex3, node.Triangle.Vertex2, node.Triangle.Vertex1);
            pointsOfNeighbors.Add(point);

            foreach (var candidate in candidates)
            {
                if (candidate == node) continue;

                if (pointsOfNeighbors.Any(p => candidate.Triangle.IsHit(p)))
                {
                    neighbors.Add(candidate);
                }

                if (neighbors.Count == NEIGHBORS_COUNT) break;
            }

            return neighbors;
        }

        private static string GetIndexPath(string output)
        {
            return output.Substring(0, output.Length - 4) + "_Index.dat";
        }


        private class DescriptorDb
        {
            public int PropertiesCount;
            public Dictionary<string, Tuple<TypeCode, byte>> Properties;
            public int PointSize;
            public int Size;

            public DescriptorDb()
            {
                this.Properties = new Dictionary<string, Tuple<TypeCode, byte>>();
                PropertiesCount = 0;
                PointSize = 0;
                Size = 0;
            }
        }
    }
}
