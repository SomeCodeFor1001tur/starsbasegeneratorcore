﻿using System;
using System.Collections.Generic;
using System.IO;

namespace StarsBaseGenerator.Core
{
    static class Finder
    {
        public static void ShowDiscriptor(string path)
        {
            using (var reader = new BinaryReader(File.Open(path, FileMode.Open)))
            {
                int propertyCount = reader.ReadInt32();
                Console.WriteLine("count of propertys:   " + propertyCount);
                for (int i = 0; i < propertyCount; i++)
                {
                    Console.WriteLine(reader.ReadString() + "  ----->  " + (TypeCode)reader.ReadByte());
                }
                Console.WriteLine("point size is: " + reader.ReadInt32());
                Console.WriteLine("desc size" + reader.ReadInt32());
            }
        }
        public static int GetDescriptorSize(string path)
        {
            int descSize = 0;
            using (var reader = new BinaryReader(File.Open(path, FileMode.Open)))
            {
                int propertyCount = reader.ReadInt32();
                for (int i = 0; i < propertyCount; i++)
                {
                    reader.ReadString();
                    reader.ReadByte();
                }
                reader.ReadInt32();
                descSize = reader.ReadInt32();
            }
            return descSize;
        }

        public static int SizeOfPointInDB(string path)
        {
            int pointSize = 0;
            using (var reader = new BinaryReader(File.Open(path, FileMode.Open)))
            {
                int propertyCount = reader.ReadInt32();
                for (int i = 0; i < propertyCount; i++)
                {
                    reader.ReadString();
                    reader.ReadByte();
                }
                pointSize = reader.ReadInt32();
            }
            return pointSize;
        }

        public static List<SphericalPoint> GetPointsFromTriangle(int id, string indexPath, string catalogPath)
        {
            int start, finish;
            using (var fstream = File.OpenRead(indexPath))
            {
                int sizeOfRecordInIndexFile = 20;
                int offset = (id - 1) * sizeOfRecordInIndexFile;
                fstream.Seek(offset, SeekOrigin.Begin);

                byte[] output = new byte[8];
                fstream.Read(output, 0, output.Length);

                start = BitConverter.ToInt32(output, 0);
                finish = BitConverter.ToInt32(output, 4);

            }

            int descSize = GetDescriptorSize(catalogPath);
            int pointSize = SizeOfPointInDB(catalogPath);

            var points = new List<SphericalPoint>();

            using (FileStream fstream = File.OpenRead(catalogPath))
            {
                int offset = start * pointSize + descSize;

                fstream.Seek(offset, SeekOrigin.Begin);


                int size = (finish - start) * pointSize;
                byte[] output = new byte[size];
                fstream.Read(output, 0, output.Length);



                int startIndex = 0;
                for (int j = 0; j < finish - start; j++)
                {
                    points.Add(new SphericalPoint(BitConverter.ToDouble(output, startIndex), BitConverter.ToDouble(output, startIndex + 8)));
                    startIndex += pointSize;

                    double R1 = BitConverter.ToDouble(output, startIndex + 16);
                    double R2 = BitConverter.ToDouble(output, startIndex + 24);
                    double R3 = BitConverter.ToDouble(output, startIndex + 32);
                }
            }

            return points;
        }
    }
}
