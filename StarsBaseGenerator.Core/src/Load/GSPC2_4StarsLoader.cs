﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace StarsBaseGenerator.Core
{
    public class Gspc24StarsLoader : StarsLoader
    {
        private const int Size = 554007;

        public override List<SphericalPointWithProperties> Load(string path)
        { 
            var points = new List<SphericalPointWithProperties>(Size);

            using (var sr = new StreamReader(path))
            {
                var splitChars = new[] { ' ' };
                while (sr.Peek() >= 0)
                {                    
                    string[] starParams = sr.ReadLine().Split(splitChars, StringSplitOptions.RemoveEmptyEntries);
                    SphericalPointWithProperties point = new SphericalPointWithProperties(Double.Parse(starParams[2], CultureInfo.InvariantCulture), 
                                                                                            Double.Parse(starParams[1], CultureInfo.InvariantCulture));

                    point.SetProperty("magnitude_B_aperture", Double.Parse(starParams[4], CultureInfo.InvariantCulture));
                    point.SetProperty("magnitude_V_aperture", Double.Parse(starParams[9], CultureInfo.InvariantCulture));
                    point.SetProperty("magnitude_R_aperture", Double.Parse(starParams[14], CultureInfo.InvariantCulture));

                    points.Add(point);
                }
            }
            return points;
        }        
    }
}
