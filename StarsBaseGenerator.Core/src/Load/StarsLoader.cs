﻿using System.Collections.Generic;

namespace StarsBaseGenerator.Core
{
    public abstract class StarsLoader
    {
        public abstract List<SphericalPointWithProperties> Load(string path);
    }
    
}
