﻿using System.Globalization;

namespace StarsBaseGenerator.Core
{
    public class SphericalPoint
    {
        public double Radius { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public SphericalPoint(double longitude, double latitude, double radius = 1)
        {
            Longitude = longitude;
            Latitude = latitude;
            Radius = radius;
        }

        public override string ToString()
        {
            return Longitude.ToString(CultureInfo.InvariantCulture) + ' ' + Latitude.ToString(CultureInfo.InvariantCulture);
        }
    }
}
