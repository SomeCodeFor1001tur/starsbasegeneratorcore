﻿using System.Collections.Generic;

namespace StarsBaseGenerator.Core
{
    public class SphericalPointWithProperties : SphericalPoint
    {
        public Dictionary<string, dynamic> Properties { get; set; }

        public SphericalPointWithProperties(double longitude, double latitude, double radius = 1) 
            : base(longitude, latitude, radius) 
        {
            Properties = new Dictionary<string, dynamic>(); 
        }

        public void SetProperty(string name, dynamic value)
        {
            Properties.Add(name, value);
        }

        public object GetProperty(string name)
        {
            return Properties[name];
        }
    }
}
