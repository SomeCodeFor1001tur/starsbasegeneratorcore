﻿using System.Windows.Media.Media3D;

namespace StarsBaseGenerator.Core
{
    public class SphericalTriangle
    {
        public SphericalPoint Vertex1 { get; set; }
        public SphericalPoint Vertex2 { get; set; }
        public SphericalPoint Vertex3 { get; set; }

        public SphericalTriangle(SphericalPoint p1, SphericalPoint p2, SphericalPoint p3)
        {
            Vertex1 = p1;
            Vertex2 = p2;
            Vertex3 = p3;
        }

        public bool IsHit(SphericalPoint point)
        {
            Point3D v1 = Util.ConvertSphericalToCartesian(Vertex1);
            Point3D v2 = Util.ConvertSphericalToCartesian(Vertex2);
            Point3D v3 = Util.ConvertSphericalToCartesian(Vertex3);
            Point3D p = Util.ConvertSphericalToCartesian(point);

            double determinant1 = Util.CalculateDeterminant3X3(v1, v2, v3);
            double determinant2 = Util.CalculateDeterminant3X3(p, v2, v3);
            double determinant3 = Util.CalculateDeterminant3X3(v1, p, v3);
            double determinant4 = Util.CalculateDeterminant3X3(v1, v2, p);

            return Util.IsSameSign(determinant1, determinant2, determinant3, determinant4);
        }
    }
}
