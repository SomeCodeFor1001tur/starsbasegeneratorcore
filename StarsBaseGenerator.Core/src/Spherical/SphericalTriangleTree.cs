﻿using System.Collections.Generic;

namespace StarsBaseGenerator.Core
{
    public class SphericalTriangleTree
    {
        public Node MainRoot;

        private Dictionary<int, List<Node>> _levels;

        private int _levelsCount;

        private void Initialize()
        {
            SphericalPoint[] points = new SphericalPoint[]
            {
                new SphericalPoint(0, 90),
                new SphericalPoint(-180, 26.5650511771),
                new SphericalPoint(-108, 26.5650511771),
                new SphericalPoint(-36, 26.5650511771),
                new SphericalPoint(36, 26.5650511771),
                new SphericalPoint(108, 26.5650511771),
                new SphericalPoint(-144, -26.5650511771),
                new SphericalPoint(-72, -26.5650511771),
                new SphericalPoint(0, -26.5650511771),
                new SphericalPoint(72, -26.5650511771),
                new SphericalPoint(144, -26.5650511771),
                new SphericalPoint(0, -90)
            };

            MainRoot = new Node();

            var roots = new List<Node>()
            {
                new Node() {Triangle = new SphericalTriangle(points[0], points[5], points[4])},
                new Node() {Triangle = new SphericalTriangle(points[0], points[4], points[3])},
                new Node() {Triangle = new SphericalTriangle(points[0], points[3], points[2])},
                new Node() {Triangle = new SphericalTriangle(points[0], points[2], points[1])},
                new Node() {Triangle = new SphericalTriangle(points[0], points[1], points[5])},
                new Node() {Triangle = new SphericalTriangle(points[2], points[1], points[6])},
                new Node() {Triangle = new SphericalTriangle(points[6], points[10], points[1])},
                new Node() {Triangle = new SphericalTriangle(points[1], points[5], points[10])},
                new Node() {Triangle = new SphericalTriangle(points[5], points[10], points[9])},
                new Node() {Triangle = new SphericalTriangle(points[2], points[3], points[7])},
                new Node() {Triangle = new SphericalTriangle(points[3], points[4], points[8])},
                new Node() {Triangle = new SphericalTriangle(points[8], points[4], points[9])},
                new Node() {Triangle = new SphericalTriangle(points[8], points[3], points[7])},
                new Node() {Triangle = new SphericalTriangle(points[6], points[10], points[11])},
                new Node() {Triangle = new SphericalTriangle(points[10], points[11], points[9])},
                new Node() {Triangle = new SphericalTriangle(points[11], points[9], points[8])},
                new Node() {Triangle = new SphericalTriangle(points[11], points[8], points[7])},
                new Node() {Triangle = new SphericalTriangle(points[11], points[7], points[6])},
                new Node() {Triangle = new SphericalTriangle(points[4], points[5], points[9])},
                new Node() {Triangle = new SphericalTriangle(points[2], points[6], points[7])}
            };
            foreach (var root in roots)
                root.Parent = MainRoot;

            MainRoot.Childs = roots; 
        }

        public SphericalTriangleTree(int levelsCount)
        {
            Initialize();
            this._levelsCount = levelsCount;      

            foreach (var root in MainRoot.Childs)
            {
                Split(root, levelsCount);
            }

            _levels = new Dictionary<int, List<Node>>();
            CalculateLevels(MainRoot);
            CalculateNodeId();
        }

        public Dictionary<int, List<Node>> Levels {
            get  { return _levels; } 
        }

        private void CalculateLevels(Node node, int level=0)
        {
            if (!_levels.ContainsKey(level)) _levels.Add(level, new List<Node>());
            _levels[level].Add(node);

            foreach (var child in node.Childs)
            {
                CalculateLevels(child, level + 1);
            }
        }

        private void CalculateNodeId()
        {
            int id = 0;

            foreach (var level in Levels)
                foreach (var node in level.Value)
                    node.Id = id++;
        }

        private void Split(Node node, int level=1)
        {
            if (level == 0) return;

            SphericalTriangle triangle = node.Triangle;

            node.AddChild(new Node()
            {
                Triangle =
                    new SphericalTriangle(Util.GetMiddlePoint(triangle.Vertex1, triangle.Vertex2), triangle.Vertex2,
                        Util.GetMiddlePoint(triangle.Vertex2, triangle.Vertex3))
            });

            node.AddChild(new Node()
            {
                Triangle =
                    new SphericalTriangle(triangle.Vertex1, Util.GetMiddlePoint(triangle.Vertex1, triangle.Vertex2),
                        Util.GetMiddlePoint(triangle.Vertex1, triangle.Vertex3))
            });

            node.AddChild(new Node()
            {
                Triangle =
                    new SphericalTriangle(Util.GetMiddlePoint(triangle.Vertex1, triangle.Vertex2),
                        Util.GetMiddlePoint(triangle.Vertex2, triangle.Vertex3),
                        Util.GetMiddlePoint(triangle.Vertex1, triangle.Vertex3))
            });

            node.AddChild(new Node()
            {
                Triangle =
                    new SphericalTriangle(Util.GetMiddlePoint(triangle.Vertex1, triangle.Vertex3),
                        Util.GetMiddlePoint(triangle.Vertex2, triangle.Vertex3),
                        triangle.Vertex3)
            });

            if (level > 1)
            {
                foreach (var child in node.Childs)
                {
                    Split(child, level - 1);
                }
            }
        }
                
        public Node FindLastHitNode(SphericalPoint point)
        {
            Node node = MainRoot;

            while (node.HasChild())
            {
                foreach (var child in node.Childs)
                {
                    if (child.Triangle.IsHit(point))
                    {
                        node = child;
                        break;
                    }
                }
            }
            return node;
        }

        public int FindLastHitNodeId(SphericalPoint point)
        {
            return FindLastHitNode(point).Id;
        }
    }

    public class Node
    {
        public int Id { get; set; }

        public Node Parent { get; set; }

        public SphericalTriangle Triangle { get; set; }

        public List<Node> Childs = new List<Node>();

        public void AddChild(Node child)
        {
            child.Parent = this;
            Childs.Add(child);
        }
        public bool HasChild()
        {
            return Childs.Count != 0;
        }
    }
}
