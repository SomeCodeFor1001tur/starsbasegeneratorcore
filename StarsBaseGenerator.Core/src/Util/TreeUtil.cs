﻿using System.Collections.Generic;

namespace StarsBaseGenerator.Core
{
    public static class TreeUtil
    {
        public static Node GetLeftmostLeaf(Node node)
        {
            while (node.HasChild())
                node = node.Childs[0];
            return node;
        }
        public static Node GetRightmostLeaf(Node node)
        {
            while (node.HasChild())
                node = node.Childs[node.Childs.Count - 1];
            return node;
        }
       
        public static void GetLeafs(Node root, List<Node> leafs)
        {
            if (root.HasChild())
            {
                foreach (var child in root.Childs)
                {
                    GetLeafs(child, leafs);
                }
            }
            else leafs.Add(root);
        }
    }
}
