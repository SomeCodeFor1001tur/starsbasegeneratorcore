﻿using System;
using System.Collections.Generic;

namespace StarsBaseGenerator.Core
{
    public static class Util
    {
        public static Point3D ConvertSphericalToCartesian(SphericalPoint point, double radius=1)
        {
            return new Point3D()
            {
                X=radius*Math.Cos(ConvertGradToRad(point.Latitude))*Math.Cos(ConvertGradToRad(point.Longitude)),
                Y=radius * Math.Cos(ConvertGradToRad(point.Latitude)) * Math.Sin(ConvertGradToRad(point.Longitude)),
                Z=radius*Math.Sin(ConvertGradToRad(point.Latitude))
            };

        }

        public static SphericalPoint ConvertCartesianToSpherical(Point3D point) 
        {
            double radius = Math.Sqrt(point.X*point.X + point.Y*point.Y + point.Z*point.Z);

            return new SphericalPoint(ConvertRadToGrad(Math.Atan2(point.Y , point.X)), 
                ConvertRadToGrad(Math.Asin(point.Z / radius)), radius);
        }

        public static double ConvertGradToRad(double grad)
        {
            return grad/180*Math.PI;
        }

        public static double ConvertRadToGrad(double rad)
        {
            return rad * 180 / Math.PI;
        }

        public static double CalculateDeterminant3X3(Point3D a, Point3D b, Point3D c)
        {
            return a.X*b.Y*c.Z - a.X*b.Z*c.Y + a.Y*b.Z*c.X - a.Y*c.Z*b.X + a.Z*b.X*c.Y - a.Z*c.X*b.Y;
        }

        public static SphericalPoint GetMiddlePoint(SphericalPoint p1, SphericalPoint p2)
        {
            Point3D point1 = ConvertSphericalToCartesian(p1);
            Point3D point2 = ConvertSphericalToCartesian(p2);

            Point3D midPoint = new Point3D((point1.X + point2.X) / 2, (point1.Y + point2.Y) / 2, (point1.Z + point2.Z) / 2);

            return ConvertCartesianToSpherical(midPoint);              
        }

        public static List<SphericalPoint> GetListOfRandomSphericalPoints(int count)
        {
            List<SphericalPoint> points = new List<SphericalPoint>(count);

            Random random = new Random();

            for (int i = 0; i < count; i++)
            {
                points.Add(new SphericalPoint(random.NextDouble() * 360, random.NextDouble() * 360));
            }

            return points;
        }

        public static bool IsSameSign(params double[] args)
        {
            double sumOfAbs = 0,
                absOfSum = 0;
            foreach (var arg in args)
            {
                sumOfAbs += Math.Abs(arg);
                absOfSum += arg;
            }
            absOfSum = Math.Abs(absOfSum);

            return Math.Abs(sumOfAbs - absOfSum) < 0.0001;
        }

        public static SphericalPoint GetPointOnMedianOutsideTriangleInFrontOfOppositePoint(SphericalPoint lh, SphericalPoint rh, SphericalPoint opposite)
        {
            var middle = GetMiddlePoint(lh, rh);

            Point3D middleCartesian = ConvertSphericalToCartesian(middle);
            Point3D oppositeCartesian = ConvertSphericalToCartesian(opposite);

            Vector3D medianFromOpposite = new Vector3D(middleCartesian.X - oppositeCartesian.X, middleCartesian.Y - oppositeCartesian.Y, middleCartesian.Z - oppositeCartesian.Z);

            Vector3D oneThirdMedian = Vector3D.Divide(medianFromOpposite, 3);

            return ConvertCartesianToSpherical(Vector3D.Add(oneThirdMedian, middleCartesian));
        }

    }

}
